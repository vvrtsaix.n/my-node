FROM mhart/alpine-node:latest

WORKDIR /app
COPY package.json /app/
RUN yarn install --producation
COPY index.js /app/

ENTRYPOINT [ "node", "index.js" ]
