const express = require("express");
const app = express();
const port = 3000;

let live = false;
let health = false;

app.get("/", (_req, res) => {
  res.send("Hello World!");
});

app.get("/live", (_req, res) => {
  if (live) {
    return res.status(200).send({ success: true });
  }
  return res.status(501).send({ success: false });
});

app.get("/health", (_req, res) => {
  if (health) {
    return res.status(200).send({ success: true });
  }
  return res.status(501).send({ success: false });
});

setTimeout(() => {
  live = true;
}, 120000);

setTimeout(() => {
  health = true;
}, 240000);

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
